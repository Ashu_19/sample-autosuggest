import {Injectable, OnInit} from '@angular/core';
import {Observable, of} from 'rxjs';
import {HttpClient} from '@angular/common/http';
import {exhaustMap, shareReplay} from 'rxjs/operators';

@Injectable()
export class ShoppingListService {

  cachedResponse = {};

  constructor(private httpClient: HttpClient) {
  }

  getSuggestions(value: string): Observable<{ name: string }[]>{

    if (this.cachedResponse[value]) {
      console.log('Returning cached response');
      return this.cachedResponse[value];
    }

    console.log('Data not found in cache, Hitting API!!');
    this.cachedResponse[value] = this.httpClient
      .get<{name: string}[]>('http://localhost:9000/api/shopping-list')
      .pipe(
        exhaustMap(response => {
          return of(response.filter(res => {
            return res.name.startsWith(value);
          }));
        }), shareReplay(1)); // Since the API calls are async and will be put in the browser task queue we need
                                      // shareReplay to prevent other subscriptions (subsequent calls ) to hit the API.

    return this.cachedResponse[value];
  }
}
