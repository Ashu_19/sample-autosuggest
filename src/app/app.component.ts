import {Component, OnInit, ViewChild} from '@angular/core';
import {debounceTime, distinctUntilChanged, filter, pluck, tap} from 'rxjs/operators';
import {NgForm} from '@angular/forms';
import {ShoppingListService} from './shopping-list.service';
import {Subject} from 'rxjs';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css'],
  providers: [ShoppingListService]
})
export class AppComponent implements OnInit{

  @ViewChild('f', {static: true}) form: NgForm;
  suggestions: {name: string}[];
  clearCacheTimeout: any;

  constructor(private shoppingListService: ShoppingListService) {
  }

  ngOnInit(): void {
    this.form.valueChanges.pipe(debounceTime(350),
      distinctUntilChanged(),
      pluck('name'),
      tap(vl => {
        if (vl === '') {
          this.suggestions = null;
        }
      }),
      filter(val => val !== '')).subscribe(data => {
      console.log(data);
      this.getSuggestions(data);
    });
  }

  getSuggestions(value: string): void {
    if (value && value !== '') {
      this.shoppingListService.getSuggestions(value).subscribe( resp => {
        resp?.length > 0 ? this.suggestions = resp : this.suggestions = null;

        // tslint:disable-next-line:max-line-length
        // Clearing up our cache after 1 minutes, this is to prevent our cache from growing infinitely, on browser page refresh cache will also automatically be cleared.

        if (!this.clearCacheTimeout) {
          this.clearCacheTimeout = setTimeout(() => {
            this.shoppingListService.cachedResponse = {};
            this.clearCacheTimeout = null;
          }, 60000);
        }
      });
    }
  }

  setValue($event: any): void {
    this.form.controls.name.setValue($event.target.textContent, {emitEvent: false}); // cannot bind with model
    this.suggestions = null;
  }
}
