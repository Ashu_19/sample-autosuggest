import {Request, Response} from 'express';
import {SHOPPING_LIST} from './db-data';

export function getShoppingList(req: Request, res: Response) {
    setTimeout(() => {
      res.status(200).json(Object.values(SHOPPING_LIST));
    }, 120);
}
