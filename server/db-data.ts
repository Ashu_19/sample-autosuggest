export const SHOPPING_LIST: any = [
  {
    name: 'Cabbage'
  },
  {
    name: 'Carrot'
  },
  {
    name: 'Cauliflower'
  },
  {
    name: 'Celeriac'
  },
  {
    name: 'Celery'
  },
  {
    name: 'Chard'
  },
  {
    name: 'Chilli'
  }
];

