import * as express from 'express';
import {Application} from 'express';
import {getShoppingList} from './get-shopping-list.route';


const bodyParser = require('body-parser');

const app: Application = express();

app.use(bodyParser.json());
app.use((req, res, next) => {
  res.header('Access-Control-Allow-Origin', '*');
  res.header('Access-Control-Allow-Headers', 'Origin, X-Requested-With, Content-Type, Accept');
  next();
});

app.route('/api/shopping-list').get(getShoppingList);

const httpServer: any = app.listen(9000, () => {
    console.log('HTTP REST API Server running at http://localhost:' + httpServer.address().port);
});




